# descomplica
Criei esse repositório para guardar os helms das aplicações fiz um para o ambiente de homologação que fica no namespace de hmlg e o de produção no namespace de prod, poderia ficar mais reutilizável colocando 1 chart default e só mudando o values para não ficar repetindo, mas fiz desse jeito para otimizar o tempo. 
Criei uma pipeline que contem dois estágios, um para validar os values do helm e outra para fazer o apply de forma manual.

## Aqui tem a parte da validação do value do helms

![image](docs/validate.PNG)




## Helm do ambiente de homologação

![image](docs/hmlg.PNG)


## Helm do ambiente de produção

![image](docs/prod.PNG)


## Imagens dos pods em funcionamento

![image](docs/geral.PNG)


## Pipeline

![image](docs/pipeline.PNG)


